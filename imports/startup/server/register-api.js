import '/imports/api/app/methods.js'

import '/imports/api/users/methods.js'
import '/imports/api/users/server/methods.js'
import '/imports/api/users/server/publications.js'

import '/imports/api/notes/methods.js'
import '/imports/api/notes/server/methods.js'
import '/imports/api/notes/server/publications.js'
import '/imports/api/notes/server/routes.js'

import '/imports/api/files/server/files.js'
import '/imports/api/files/server/publications.js'
import '/imports/api/files/server/methods.js'

import '/imports/api/tags/tags.js'
import '/imports/api/tags/server/methods.js'

import '/imports/api/bot/server/methods.js'
import '/imports/api/bot/server/routes.js'